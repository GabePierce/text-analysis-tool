# This program displays an empty windowadadad.
import tkinter, string, csv, math
class MyClass:
    def __init__(self):
        # Create parent window and set the title to 'Text Analysis Tool'
        self.main_window = tkinter.Tk()
        self.main_window.title('Text Analysis Tool')
        # Set the min size to be 400w 500h
        self.main_window.minsize(400,500)
        
        # Create the radiobuttons to pick from either
        # front to back or back to front
        self.ad_frame = tkinter.Frame(self.main_window)
        self.ad_selection = tkinter.IntVar()
        self.ad_selection.set(1)
        self.a_radio = tkinter.Radiobutton(self.ad_frame,text='front to back',variable=self.ad_selection,value=1)
        #front to back is == 1
        self.b_radio = tkinter.Radiobutton(self.ad_frame,text='back to front',variable=self.ad_selection,value=2)
        #back to front is == 2
        self.radio_label = tkinter.Label(self.ad_frame,text='How would you like your data sorted?')
        # Packing the frame and it's buttons
        self.ad_frame.pack(anchor='w')
        self.radio_label.pack(side='left')
        self.a_radio.pack(side='left')
        self.b_radio.pack(side='right')
        
        
        # Adding a Select File option.
        self.select_file_label_frame = tkinter.Frame(self.main_window)
        self.select_file_frame = tkinter.Frame(self.main_window)
        self.select_file_label = tkinter.Label(self.select_file_label_frame,text='Please Select a Text file',font=('Helvetica', 12, 'bold'))
        self.select_file_button = tkinter.Button(self.select_file_frame,text='Select File',command=self.select_file)
        self.path_variable = tkinter.StringVar()
        self.path_variable.set('texts\\test.txt')
        self.select_file_textEntry = tkinter.Entry(self.select_file_frame,textvariable=self.path_variable)
        # Packing Select File option.
        self.select_file_label_frame.pack(anchor='w')
        self.select_file_frame.pack(anchor='w')
        self.select_file_label.pack(side='left')
        self.select_file_button.pack(side='left')
        self.select_file_textEntry.pack(side='left')
        
        # Set up button to count the unique words and export it to
        # the results.txt or whatever I end up naming it...
        self.count_unique_button_label_frame = tkinter.Frame(self.main_window)
        self.count_unique_button_frame = tkinter.Frame(self.main_window)
        self.count_unique_button_label = tkinter.Label(self.count_unique_button_label_frame,text='Count Repititions of All Words',font=('Helvetica', 12, 'bold'))
        self.count_unique_button_txt = tkinter.Button(self.count_unique_button_frame,text='Export to results.txt',command=self.export_repetitions_text)
        self.count_unique_button_csv = tkinter.Button(self.count_unique_button_frame,text='Export to results.csv',command=self.export_repetitions_csv)
        # Packing the frame, labels and buttons up
        self.count_unique_button_label_frame.pack(anchor='w')
        self.count_unique_button_frame.pack(anchor='w')
        self.count_unique_button_label.pack(side='left')
        self.count_unique_button_txt.pack(side='right')
        self.count_unique_button_csv.pack(side='right')
        # HTML and CSS stuff
        self.count_unique_button_html_frame = tkinter.Frame(self.main_window)
        self.count_unique_button_html = tkinter.Button(self.count_unique_button_html_frame,text='Display in HTML and CSS',command=self.export_repetitions_html)
        self.count_unique_button_variable = tkinter.StringVar()
        self.count_unique_button_variable.set('500')
        self.count_unique_button_label = tkinter.Label(self.count_unique_button_html_frame,text='Font-size Multiplyer')
        self.count_unique_button_entry = tkinter.Entry(self.count_unique_button_html_frame,textvariable=self.count_unique_button_variable,width='7')
        self.bg_color = tkinter.StringVar()
        self.fg_color = tkinter.StringVar()
        self.bg_color.set('#FFFFFF')
        self.fg_color.set('#000000')

        self.count_unique_button_fg_color = tkinter.Button(self.count_unique_button_html_frame,text='Choose Foreground Color',command=self.choose_fg_color)
        self.count_unique_button_bg_color = tkinter.Button(self.count_unique_button_html_frame,text='Choose Background Color',command=self.choose_bg_color)
        # Packing the html up
        self.count_unique_button_html_frame.pack(anchor='w')
        self.count_unique_button_html.pack(side='left')
        self.count_unique_button_label.pack(side='left')
        self.count_unique_button_entry.pack(side='left')
        self.count_unique_button_fg_color.pack(side='left')
        self.count_unique_button_bg_color.pack(side='left')
        # Set Up Count Words
        self.count_words_header_frame = tkinter.Frame(self.main_window)
        self.count_words_frame = tkinter.Frame(self.main_window)
        self.count_words_Label = tkinter.Label(self.count_words_header_frame,text='Get Word Count',font=('Helvetica', 12, 'bold'))
        self.count_words_button = tkinter.Button(self.count_words_frame,text='Get Word Count',command=self.get_word_count)
        self.count_unique_words_button = tkinter.Button(self.count_words_frame,text='Get Unique Word Count',command=self.get_unique_word_count)
        self.count_words_string_variable = tkinter.StringVar()
        self.count_words_string_variable.set('Your results will appear here.')
        self.count_words_footer_frame = tkinter.Frame(self.main_window)
        self.count_words_results = tkinter.Entry(self.count_words_footer_frame,textvariable=self.count_words_string_variable,width='50')
        # Pack Count Words
        self.count_words_header_frame.pack(anchor='w')
        self.count_words_frame.pack(anchor='w')
        self.count_words_Label.pack(side='left')
        self.count_words_button.pack(side='left')
        self.count_unique_words_button.pack(side='left')
        self.count_words_footer_frame.pack(anchor='w')
        self.count_words_results.pack(side='left')

        # Set up Count specific word
        self.count_spec_word_lframe = tkinter.Frame(self.main_window)
        self.count_spec_word_label = tkinter.Label(self.count_spec_word_lframe,text='Get Count of Specific Word',font=('Helvetica', 12, 'bold'))
        self.count_spec_word_frame = tkinter.Frame(self.main_window)
        self.count_spec_word_var = tkinter.StringVar()
        self.count_spec_word_var.set('Enter the word or phrase you would like to count')
        self.count_spec_word_entry = tkinter.Entry(self.count_spec_word_frame,textvariable=self.count_spec_word_var,width='45')
        self.csw_case_sensitive_var = tkinter.IntVar()
        self.count_spec_word_cb = tkinter.Checkbutton(self.count_spec_word_frame,text='Case Sensitive',variable=self.csw_case_sensitive_var)
        self.count_spec_word_rframe = tkinter.Frame(self.main_window)
        self.count_spec_word_button = tkinter.Button(self.count_spec_word_rframe,text='Count',command=self.get_specific_word_count)
        self.count_spec_word_rvar = tkinter.StringVar()
        self.count_spec_word_rvar.set('Your results will appear here.')
        self.count_spec_word_rentry = tkinter.Entry(self.count_spec_word_rframe,textvariable=self.count_spec_word_rvar,width='50')
        # Pack Count Specific word
        self.count_spec_word_lframe.pack(anchor='w')
        self.count_spec_word_label.pack(side='left')
        self.count_spec_word_frame.pack(anchor='w')
        self.count_spec_word_entry.pack(side='left')
        self.count_spec_word_cb.pack(side='left')
        self.count_spec_word_rframe.pack(anchor='w')
        self.count_spec_word_button.pack(side='left')
        self.count_spec_word_rentry.pack(side='left')

        # Get frequency of specific word
        self.freq_labelframe = tkinter.Frame(self.main_window)
        self.freq_label = tkinter.Label(self.freq_labelframe,text='Get Frequency of Word',font=('Helvetica', 12, 'bold'))
        # The search box
        self.freq_searchframe = tkinter.Frame(self.main_window)
        self.freq_search_var = tkinter.StringVar()
        self.freq_search_var.set('The word or phrase to find the frequency of')
        self.freq_search_n_var = tkinter.StringVar()
        self.freq_search_n_var.set('100')
        self.freq_entry = tkinter.Entry(self.freq_searchframe,textvariable=self.freq_search_var,width='45')
        self.freq_n_entry = tkinter.Entry(self.freq_searchframe,textvariable=self.freq_search_n_var,width='7')
        self.freq_cb_var = tkinter.IntVar()
        self.freq_search_cb = tkinter.Checkbutton(self.freq_searchframe,variable = self.freq_cb_var, text='Case sensitive')
        # The 'export as's
        self.freq_exportframe = tkinter.Frame(self.main_window)
        self.freq_export_txt = tkinter.Button(self.freq_exportframe,text='Export to results.txt',command=self.export_frequency_text)
        self.freq_export_csv = tkinter.Button(self.freq_exportframe,text='Export to results.csv',command=self.export_frequency_csv)
        # The averages
        self.freq_averageframe = tkinter.Frame(self.main_window)
        self.freq_averagebutton = tkinter.Button(self.freq_averageframe,text='Get average frequency',command=self.get_average_frequency)
        self.freq_averagevar = tkinter.StringVar()
        self.freq_averagevar.set('Your results will appear here')
        self.freq_averagereults = tkinter.Entry(self.freq_averageframe,textvariable = self.freq_averagevar, width=50)
        # Pack all of the above
        self.freq_labelframe.pack(anchor='w')
        self.freq_label.pack(side='left')
        self.freq_searchframe.pack(anchor='w')
        self.freq_entry.pack(side='left')
        self.freq_n_entry.pack(side='left')
        self.freq_search_cb.pack(side='left')
        self.freq_exportframe.pack(anchor='w')
        self.freq_export_txt.pack(side='left')
        self.freq_export_csv.pack(side='left')
        self.freq_averageframe.pack(anchor='w')
        self.freq_averagebutton.pack(side='left')
        self.freq_averagereults.pack(side='left')
        # Test to see if we can import the matplotlib, and in the same breath set up stuff
        # to use the matplotlib.
        try:
            # If you can get matplot lib, build tkinter stuff
            import matplotlib
            self.matplotlib_label_frame = tkinter.Frame(self.main_window)
            self.matplotlib_label = tkinter.Label(self.matplotlib_label_frame,text='MatPlotLib Chart',font=('Helvetica', 12, 'bold'))
            self.matplotlib_frame = tkinter.Frame(self.main_window)
            self.matplotlib_button = tkinter.Button(self.matplotlib_frame,text='Display results.csv in a Chart',command=self.export_to_matplotlib)
            self.barg_lineg = tkinter.IntVar()
            self.barg_lineg.set(1)
            self.matplotlib_radio_barg = tkinter.Radiobutton(self.matplotlib_frame,text='Bar Graph',variable=self.barg_lineg,value=1)
            self.matplotlib_radio_lineg = tkinter.Radiobutton(self.matplotlib_frame,text='Line Graph',variable=self.barg_lineg,value=2)
            
            self.matplotlib_label_frame.pack(anchor='w')
            self.matplotlib_label.pack(side='left')
            self.matplotlib_frame.pack(anchor='w')
            self.matplotlib_button.pack(side='left')
            self.matplotlib_radio_barg.pack(side='left')
            self.matplotlib_radio_lineg.pack(side='left')
        except ImportError:
            # If you can't get matplotlib
            # Display this text
        
            self.matplotlib_error = tkinter.Label(self.main_window,text='This application also features matplotlib functionality,\n please install matplotlib from pip to get the full effect. of this application')
            self.matplotlib_error.pack(anchor='w')

    #FILE DIALOG Opens file dialog box and saves path to path variable
    def select_file(self):
        #import filedialog
        from tkinter import filedialog
        #set filename variable to the path the user selects from file dialog box
        filename = filedialog.askopenfilename(initialdir = "/", title = "Select a File",filetypes = (("Text files", "*.txt*"), ("all files", "*.*")))
        # sets the path variable to path
        self.path_variable.set(filename)
    
    #FRONT TO BACK Reads file from file path and seperates words into list items
    def read_list_front2back(self):
        self.list_front2back = []
        # Open text file based on path chosen from file dialog box. Ensures encoding is UTF-8 to
        # avoid errors.
        text_file = open(str(self.path_variable.get()),'r',encoding='UTF-8')
        text = text_file.read()
        text_file.close
        # Removes new lines
        text = text.replace('\n',' ')
        # Sets the exit condition for the while loop to false.
        ending = False
        while not ending:
            try:
                # We add to the empty list a snipit of the text file, where the next
                # space is found. And we remove punctuation by checking if a charcheter is
                # alphanumeraic. It's kind of slow, but faster than Regex matching.
                self.list_front2back.append(''.join(e for e in text[:text.index(' ')] if e.isalnum()))
                # we remove that word and it's trailing space character.
                text = text[text.index(' ')+1:]
            except:
                # Because of the nature of this while loop eventually we are going to hit
                # index out of bounds, when that happens we just need to add the last word in
                # the text file to the list, without it's punctuation.
                self.list_front2back.append(''.join(e for e in text if e.isalnum()))
                #sets end condition to true.
                ending = True

    #BACK TO FRONT takes the front to back and reverses it.
    def read_list_back2front(self):
        self.list_back2front = []
        # Calls the front to back function in case we haven't run it yet.
        self.read_list_front2back()
        front2back_length = len(self.list_front2back)-1
        for v in range(front2back_length+1):
            # adds value of front to back v - the length of the list.
            self.list_back2front.append(self.list_front2back[front2back_length - v])

    #GET LIST calls functions based on selection.
    def get_list(self):
        if self.ad_selection.get() == 1:
            self.read_list_front2back()
            self.the_list = self.list_front2back
        else:
            self.read_list_back2front()
            self.the_list = self.list_back2front
        #returns the selected list.
        return self.the_list

    #GET UNIQUE COUNT gets a 2d list of the word in lowercase
    #letters and how many times it occurs in the text.
    def export_repetitions(self):
        # Calls the list function and gets returned value.
        the_list = self.get_list()
        # Gets every word in the text and makes it lowercase
        the_unique_list = [x.lower() for x in the_list]
        # Only has unique values as per the nature of a set.
        the_unique_list = list(set(the_unique_list))
        # Gets every word in the text and makes it lowercase for
        # sorting purposes
        the_lower_list = [x.lower() for x in the_list]
        # Sorts the text according to the order of the 
        # get list function. Because a set does not have
        # a particular order. It uses list comprehension.
        the_unique_list = sorted(the_unique_list, key=lambda x: the_lower_list.index(x))
        
        # Sets counter to 0.
        count_of_repetitions = 0
        # Makes an empty list.
        self.list_of_repetitions = []
        # if the first word matches any of the words in the list
        # add one to the counter.
        for one_word in the_unique_list:          
            for all_words in the_list:
                # Checks for equivalents
                if one_word.lower() == all_words.lower():
                    count_of_repetitions += 1
            # Adds value to 2d list
            self.list_of_repetitions.append([one_word,count_of_repetitions])
            # resets counter
            count_of_repetitions = 0
    
    #EXPORT REPITITIONS TO TEXT <- does that
    def export_repetitions_text(self):
        # Calls function to get list of repititions
        self.export_repetitions()
        list2write = self.list_of_repetitions
        # Opens file for writting
        write_file = open('results.txt','w')
        # Writes object by object within list object the strings formatted with three tabs between
        for pair in list2write:
            write_file.write(list2write[list2write.index(pair)][0]+'\t\t\t'+str(format(list2write[list2write.index(pair)][1],'2'))+'\n')
        write_file.close()

    #EXPORT REPITITIONS to CSV
    def export_repetitions_csv(self):
        self.export_repetitions()
        # Setting up column headers
        fields = ['word','count']
        # Setting up Rows
        rows = self.list_of_repetitions
        # This is how you write CSV files
        with open('results.csv','w',newline='') as csvfile:
            csvwriter = csv.writer(csvfile)  
            csvwriter.writerow(fields)  
            csvwriter.writerows(rows)
    #EXPORT REPITITIONS TO HTTML AND CSS
    def export_repetitions_html(self):
        # HTML stuff
        html = open('results.html','w')
        # The begginning stuff
        html.write('<!DOCTYPE html>\n<html lang="en">\n<head>\n<link rel="stylesheet" href="results.css">\n<title>Results of Query</title>\n<meta charset="utf-8">\n</head>\n<body>\n<p>')
        self.export_repetitions()
        list2write = self.list_of_repetitions
        # We are writing a very long paragraph with lots of spans to get the effect I am looking for for now.
        for pair in list2write:
            html.write('<span id="'+list2write[list2write.index(pair)][0]+'"> '+list2write[list2write.index(pair)][0]+' </span> '+'\n')
        # Closing part
        html.write('\n</p>\n</body>\n</html>')
        # Closing HTML
        html.close()
        # CSS Stuff. Opening CSS file
        css = open('results.css','w')
        # Checks to see if the user typed in something that can be turned
        # Into an integer, if so, set it to the default 500
        try:
            int(self.count_unique_button_variable.get())
        except:
            self.count_unique_button_variable.set('500')
        # Write it setting the word itself to be the id name and setting the percentage that word takes up in the text file as a percentage to multiply the user inputed value by with a 
        # default of 100% being added to every single one because it is unreadable otherwise.
        css.write('p {color: '+self.fg_color.get()+'}')
        css.write('body {background-color: '+self.bg_color.get()+'}')
        for pair in list2write:
            css.write('#'+list2write[list2write.index(pair)][0]+' {font-size: '+str(list2write[list2write.index(pair)][1]/len(list2write)*int(self.count_unique_button_variable.get())+100)+'%;float: left;}\n')
        css.close()

    # Counts all of the words in the text file.
    def get_word_count(self):
        the_list = self.get_list()
        # returns the count
        self.count_words_string_variable.set('Word Count: '+str(len(the_list)))
    # Counts all of the unique words in the text file.
    def get_unique_word_count(self):
        the_list = self.get_list()
        # gets unique words we don't need to worry about the order
        the_list = list(set(the_list))
        # returns the count
        self.count_words_string_variable.set('Unique Word Count: '+str(len(the_list)))
    def get_specific_word_count(self):
        # the list
        the_list = self.get_list()
        # returns the search
        search = self.count_spec_word_var.get()
        # strips the search of whitespace
        search = search.strip()
        # Lowers the search and the list if the check button indicates the
        # user would like to do so.
        if self.csw_case_sensitive_var.get() != 1:
            the_list = [x.lower() for x in the_list]
            search = search.lower()
        # Sets the counters to 1 and 0 and sets a delimeter for the join function
        words = 1
        delimiter = ' '
        count = 0
        for i in search:
            # gets the word count in the search
            if i == ' ':
                words += 1
        for i in range(len(the_list)):
            # joins the subscripts based on the query's word count and
            # tests to see if the search matches any combination of sequencial words
            # combined with spaces.
            list_phrase = delimiter.join(the_list[i:i+words])
            if search == list_phrase:
                count += 1
        # Some if conditions to make results look nice.
        if words != 1:
            wvar = 'words ' 
        else:
            wvar = 'word '
        if count != 1:
            cvar = ' times.'
        else:
            cvar = ' time.'
        # returns results.
        self.count_spec_word_rvar.set('The '+wvar+search+' appears '+str(count)+cvar)
    def get_frequency(self):
        search = self.freq_search_var.get()
        n = int(self.freq_search_n_var.get())
        frequency_list = []
        count = 0
        the_list = self.get_list()
        if self.freq_cb_var.get() != 1:
            the_list = [x.lower() for x in the_list]
            search = search.lower()
        # Get the amount of times it will loop.
        self.iterations = math.ceil(len(the_list)/n)
        count = 0
        for every_iteration in range(self.iterations):
                for string in the_list[every_iteration*n:every_iteration*n+n]:
                    if search == string:
                        count += 1
                frequency_list.append([every_iteration*n+n,count])
                count = 0
        return frequency_list
    def export_frequency_text(self):
        # Calls function to get list of repititions
        list2write = self.get_frequency()
        # Opens file for writting
        write_file = open('results.txt','w')
        # Writes object by object within list object the strings formatted with three tabs between
        for pair in list2write:
            write_file.write(str(list2write[list2write.index(pair)][0])+'\t\t\t'+str(format(list2write[list2write.index(pair)][1],'2'))+'\n')
        write_file.close()
    def export_frequency_csv(self):
        list2write = self.get_frequency()
        # Setting up column headers
        fields = ['sample#','count']
        # Setting up Rows
        rows = list2write
        # This is how you write CSV files
        with open('results.csv','w',newline='') as csvfile:
            csvwriter = csv.writer(csvfile)  
            csvwriter.writerow(fields)  
            csvwriter.writerows(rows)
    #AVERAGE FREQUENCY
    def get_average_frequency(self):
        # Call the get_frequency list and retreive the returned list
        the_list = self.get_frequency()
        # set a summizer to catch all of the values to divide them later
        average = 0
        # for every object in the list
        for i in range(len(the_list)):
            # I want to dismantle the object by getting the index of the current item and grabbing it's count. 
            # and sum it to the average variable
            average += the_list[i][1]
        # now I want to divide the sum by the amount of times the n value in get_frequency can divide into the number of words in the text.
        average = average/self.iterations
        
        self.freq_averagevar.set('On average '+str(format(average,'.7f'))+'% of each sample is the word '+self.freq_search_var.get())
    #FOREGROUND COLOR SELECTION
    def choose_fg_color(self):
        # import ask color
        from tkinter.colorchooser import askcolor
        # ask the user to pick a color and only grab the hexadecimal value
        self.fg_color.set(askcolor(title = 'Choose Foreground Color for CSS file')[1])
    #BACKGROUND COLOR SELECTION
    def choose_bg_color(self):
        # import the ask color
        from tkinter.colorchooser import askcolor
        # ask the user to pick a color and only grab the hexadecimal value
        self.bg_color.set(askcolor(title = 'Choose Background Color for CSS file')[1])
    #GRAPHING MATPLOTLIB    
    def export_to_matplotlib(self):
        # I don't need to call a matplotlib check because this
        # function can only be called by the button in the previous
        # matplot lib check.
        import matplotlib.pyplot as plt
        # Empty list for the x axis
        x=[]
        # Empty list for the y axis
        y=[]
        # Read the CSV file using a csv.reader object
        with open('results.csv', 'r') as csvfile:
            plots= csv.reader(csvfile, delimiter=',')
            counter = 0
            # I just set a counter to determine if it is the 
            # first row. Because I don't want to graph the first row
            # in my data, and because it can cause
            # errors if I do.

            # for every row in the csv.reader object
            for row in plots:
                # I want to see if it is the first row
                if counter == 0:
                    # if it is I want to make sure next time I read it I know it isn't the first row.
                    counter += 1
                    # if we are dealing with the CSV file for counting repeting words in txt file
                    if 'word' in row[0]:
                        # I want to make note of that
                        is_word_count = True
                    else:
                        # otherwise I want to make sure I treat it like a frequency list
                        is_word_count = False
                    # regaurdless I want to make the x and y labels the contents of the first row.
                    plt.xlabel(str(row[0]))
                    plt.ylabel(str(row[1]))
                # if it isn't the first row.
                else:
                    # if we are dealing with a repitition of words CSV
                    if is_word_count:
                        # format like this
                        x.append(str(row[0]))
                        y.append(int(row[1]))
                    # if we are dealing with a frequency of words CSV
                    else:
                        # format like this
                        x.append(int(row[0]))
                        y.append(int(row[1]))
    
        # if the radio buttton for a bargraph is selected
        if self.barg_lineg.get() == 1:
            # make a bar graph
            plt.bar(x,y)
        else:
            # make a line graph
            plt.plot(x,y, marker='o') 
        # without regaurd to the type of graph rotate labels 90 degrees and set the title of the window
        plt.xticks(rotation = 90)
        plt.title('Chart from CSV')
        # Show the graph
        plt.show()


        tkinter.mainloop()
my_gui = MyClass()
