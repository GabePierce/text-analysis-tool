Text Analysis Tool
Then select.
Edit with IDLE >
Then select.
Edit with IDLE (version number)(bit version)
After that you should be able to see my code.
Then press F5.
from there you should be able to access the program's GUI.

Data sorting
You can sort the data by the order it appears (front to back)
or you can sort the data by the order it appears, from back to front.
This can change minor things like where in the list of repetitions of words.
that a word may appear.

Select a Text File
You can either paste the path of the file in the entry
box or you can click the select file button to choose a 
text file you want to gather data from.

Count Repetitions of All Words
This feature counts the number of times that a word appears.
in the text. Disregarding punctuation and capitalization.
you can export the results in either, CSV TXT or HTML linked to
a CSS file. The CSS and HTML file displays the word, and a font that
is linked to its occurrences in the text over the words in 
the text to get the weight that word hold over the entire text.
You can set the foreground and background to different colors
too. You can also change the font size multiplier to make
the font larger or smaller to fit your needs.

Get Word Count
Counts the words in a text document. Also has the option
to display the unique words in a text.

Get Count of Specific Word
gets the count of a word or phrase with the option of being
case sensitive.

Get Frequency of Word
Counts the occurrences of a word acrost a sample size of words.
You can set the value to 1 in the average frequency
to get the percent weight that that 
word holds over the entire text. PLEASE NOTE. the
samples do not end at the end of the text, rather, they end.
at the next equal sample. for instance, if I have a txt file
with 10 words and I choose to find the frequency over samples
of 6 the last sample will be taken from word 6 to word 12
please choose your sample sizes with caution. Because this
small difference could misrepresent your data.

Matplotlib Chart
If you have Matplotlib installed, you can visualize the data
in the CSV file in a graph! Very useful for a quick look at
what the data will look like when you graph in excel. You
can visualize the graph in either a Bar Graph or a Line Graph.
